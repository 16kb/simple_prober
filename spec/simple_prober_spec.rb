RSpec.describe SimpleProber::SimpleProberEntry do
  
  let(:hash_keys) { [:probe_duration, :probe_start].sort }

  before do
    @prober = SimpleProber::Prober.new("https:///gitlab.com")
  end

  it "has a version number" do
    expect(SimpleProber::VERSION).not_to be nil
  end

  it "should return a hash containing the appropriate keys" do
    expect(SimpleProber::SimpleProberEntry.probe_and_respond(@prober)).to include(*hash_keys)
  end

end
