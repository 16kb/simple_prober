RSpec.describe SimpleProber::Prober do

  before do
    @prober = SimpleProber::Prober.new("https://gitlab.com")
    @f_prober = SimpleProber::Prober.new("https://wrong-url.kator")
  end

  it "should return a blank instance" do
    expect(SimpleProber::Prober.new("https://gitlab.com")).to be {}
  end

  it "should raise an ArgumentError error if no parameters passed" do
    expect { SimpleProber::Prober.new() }.to raise_error(ArgumentError)
  end

  describe "#ping" do

    it "should raise a SocketError error of wrong invalid host was passed" do
      expect { @f_prober.ping }.to raise_error(SocketError)
    end

    it "should return true or false for valid hosts" do
      expect(@prober.ping).to eq(true).or(false)
    end
  end

  context "with valid host" do
    before do
      @prober.ping
    end

    describe "#duration" do      
      it "should return a float when a valid host is probed" do
        expect(@prober.duration).to be_a(Float)
      end
    end

    describe "#started_at" do
      it "should return a `%H:%M:%S` formated time string" do
        expect(@prober.started_at).to match(/^([01]?[0-9]|2[0-3])\:[0-5][0-9]\:[0-5][0-9]$/)
      end
    end
    
  end

  context "with invalid host" do
    describe "#duration" do

      it "should return 0 for invalid hosts" do
        expect(@f_prober.duration).to be(0)
      end
    
    end

    describe "#started_at" do
      it "should raise a NameError error because ping has not defined @start_time" do
        expect { @f_prober.started_at }.to raise_error(NameError)
      end
    end

  end

end