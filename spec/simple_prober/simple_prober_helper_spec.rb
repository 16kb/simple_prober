RSpec.describe SimpleProber::SimpleProberHelper do

  describe "#every" do
    it "should wait for X seconds" do
      repeated_times = -1
      do_for = 0.5.minutes
      wait_for = 2.seconds
      expected_repeat_times = (do_for / wait_for).to_i #ignoring execution time

      SimpleProber::SimpleProberHelper.every(wait_for, do_for) do |finished|
        repeated_times += 1
      end
      
      expect(repeated_times).to be(expected_repeat_times)
    
    end
  end

  describe "#every" do
    it "should repeat Y times ignoring execution time" do
      start_at = Time.now
      do_for = 0.5.minutes
      wait_for = 2.seconds
      expected_repeat_times = (do_for / wait_for).to_i #ignoring execution time

      SimpleProber::SimpleProberHelper.every(wait_for, do_for) do |finished|
        # Silence is golden
      end
      
      finish_at = (Time.now - start_at).to_i

      expect(finish_at).to eq(do_for)
    
    end
  end

end