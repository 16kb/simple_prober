RSpec.describe Numeric do
  
  start_at = 0
  loop do
    it "should return #{start_at} seconds" do
      expect(start_at.seconds).to be(start_at)
    end

    it "should return #{start_at * 60} seconds" do
      expect(start_at.minutes).to be(start_at*60)
    end

    it "should return #{start_at * 60 * 60} seconds" do
      expect(start_at.hours).to be(start_at*60*60)
    end
    
    break if start_at >= 1000
    start_at += 1
  
  end

end