
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "simple_prober/version"

Gem::Specification.new do |spec|
  spec.name          = "simple_prober"
  spec.version       = SimpleProber::VERSION
  spec.platform      = Gem::Platform::RUBY
  spec.authors       = ["Kator James"]
  spec.email         = ["kator95@gmail.com"]

  spec.summary       = %q{Simple tool for probing and reporting response time.}
  spec.description   = %q{Simple tool for probing and reporting response time.}
  spec.homepage      = "http://gitlab.com/16kb"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_runtime_dependency "thor", "~> 0.20.0"
end
