require "net/http"
require "uri"

module SimpleProber

  class SimpleProber::Prober
    # Set the host to probe
    def initialize(host)
      @host = host
      @duration = 0
    end

    def ping
      begin

        url = URI.parse(@host)
        @start_time = Time.now
        response = Net::HTTP.get(url)
        @duration = Time.now - @start_time
        if response == ""
          false
        else
          true
        end

      rescue Errno::ECONNREFUSED
        return false
      end
    end

    def duration
      @duration
    end

    def started_at
      @start_time.strftime("%H:%M:%S")
    end

  end
  
end