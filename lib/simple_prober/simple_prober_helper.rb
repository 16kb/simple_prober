require "simple_prober/numeric" # For the sake of idiomatic code

module SimpleProber
  
  class SimpleProberHelper
    def self.every(n, timeout_in_seconds = 60.seconds)

      begin
        Timeout.timeout(timeout_in_seconds) do
          loop do
            yield false if block_given?
            sleep n
          end
        end
      rescue Timeout::Error
        yield true
      end

    end
  end

end