require "thor"
require "simple_prober"

module SimpleProber
    class CLI < Thor
        
        desc "probe <host>", "Ruby gem that exposes a CLI to check the status of <host> or https://gitlab.com when <host> is not specified, and reports an average response time after probing the site every 10 seconds for a one minute"
        long_desc <<-LONGDESC
        Probes a <host> every 10 seconds for a minute and reports the average response time

        With -p option, simple_probe probe <host> will report time in the number of decimal places specified
        LONGDESC
        option :p, :default => 5, :type => :numeric, :desc => "Number of decimal places to report time in"
        option :i, :default => 10, :type => :numeric, :desc => "Interval between consecutive probes starting after response is received"
        def probe(host = "https://gitlab.com")
            SimpleProber::SimpleProberEntry.cli_handler(host, options)
        end
    end
end