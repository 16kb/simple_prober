require "simple_prober/version"
require "simple_prober/prober"
require "simple_prober/simple_prober_helper"
require "simple_prober/numeric" # For the sake of idiomatic code
require "timeout"


module SimpleProber
  
  class SimpleProberEntry
  
    def self.probe_and_respond(prober)

      probe_duration = 0
      probe_start = Time.now

      if prober.ping
        probe_duration = prober.duration
        probe_start = prober.started_at
      end

      return {
        probe_duration: probe_duration,
        probe_start: probe_start
      }

    end
      
    def self.cli_handler(host, options)
      decimal_places = options[:p]
      interval = options[:i]

      header_formater = "|%-12s|%12s|\n"
      seperator = "|" + "=" * 12 + "|" + "=" * 12 + "|\n"

      print "Probing #{host}\n"
      print seperator
      print header_formater % ["Request Time", "Duration (s)"]
      print seperator

      start_time = Time.now
      durations = [] # Array to hold response times
      prober = Prober.new(host)
      
      SimpleProberHelper.every(interval.seconds, 1.minutes) do |finished|
        
        if not finished
          result = self.probe_and_respond(prober)
          durations.push(result[:probe_duration])
          print "|#{'%-12s' % result[:probe_start]}|#{'%12s' % result[:probe_duration].round(decimal_places)}|\n"
        else
          # Calculate average
          durations_sum = durations.sum
          durations_size = durations.size
          average_duration = durations_sum.fdiv(durations_size)
          print seperator
          print "Done probing #{host}.\n"
          print "#{durations_size} probes made in #{durations_sum}\n"
          print "Average response time is %.#{decimal_places}fs\n" % average_duration
        end
      
      end

    end
  
  end

end
