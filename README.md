# SimpleProber

SimpleProber is a simple gem that exposes a cli to probe and report the average HTTP response of a server, http://gitlab.com by default

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'simple_prober'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install simple_prober

## Usage from the command line

After installing SimpleProber, you can use the gem via command line by:

    simple_prober probe [host] [options]

    host        the URL to the host you want to probe. This will defcault to https://gitlab.com
    
    Options:
        -p      precision or decimal places you want reporting time to be formatted in. Default is 5 decimal places
        -i      intervals to probe the host


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/16kb/simple_prober. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the SimpleProber project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/simple_prober/blob/master/CODE_OF_CONDUCT.md).
